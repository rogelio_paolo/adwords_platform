@extends('layouts.login')

@section('content')
<div class="login-container">
    <div class="row">
        <aside class="col-lg-3 col-md-6 col-sm-8 col-xs-8 col-lg-offset-5 col-md-offset-3 col-sm-offset-2 col-xs-offset-2">
            <div class="card">
                <article class="card-body">

<h4 class="card-title mb-4 mt-1 text-center">Adwords Platform Login</h4>

<hr />
<form method="post" action="{{ route('user.validate') }}">
    {{ csrf_field() }}
    <div class="form-group">
        <input name="" class="form-control" placeholder="Email or login" type="email" />
    </div>
    <div class="form-group">
        <input class="form-control" placeholder="******" type="password" />
    </div>                                      
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <button type="submit" class="btn btn-primary btn-block"> Login  </button>
            </div>
        </div>
        <div class="col-md-6 text-right">
        <a href="{{ route('user.validate') }}" class="btn btn-block btn-danger"> <i class="fab fa-google-plus"></i>   Login via Google+</a>
        </div>                                            
    </div>    

</form>

                </article>
            </div>
        </aside>
    </div>
</div>
@endsection