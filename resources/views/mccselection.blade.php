@extends('layouts.login')

@section('styles')
    <link href="{{ asset('css/custom.css') }}" rel="stylesheet">
@endsection

@section('content')
<div class="login-container">
    <div class="row">
        <aside class="col-lg-3 col-md-6 col-sm-8 col-xs-8 col-lg-offset-5 col-md-offset-3 col-sm-offset-2 col-xs-offset-2">
            <div class="card">
                <article class="card-body">


<div class="panel panel-default">
    <h4 class="card-title mb-4 mt-1 text-center">Select account to continue</h4>
    <hr />
    <div class="panel-body mcc-selection">
        <p class="mcc-account">
            <a href="">
                <span>Paolo Rogelio</span>
                <span class="pull-right">123-456-789</span>
            </a>
        </p>
        <p class="mcc-account">
            <a href="">
                <span>MCC 4</span>
                <span class="pull-right">123-456-789</span>
            </a>
        </p>
    </div>
</div>


                </article>
            </div>
        </aside>
    </div>
</div>
@endsection