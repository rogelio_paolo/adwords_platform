<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/test', function(){
    return view('mccselection');
});

Route::get('/', [
    'uses' => 'UserLoginController@index',
    'as'   => 'user.login'
]);

Route::get('/oauth2/login', [
    'uses' => 'UserLoginController@oauth',
    'as'   => 'user.oauth'
]);

Route::get('/validate', [
    'uses' => 'UserLoginController@oauth',
    'as'   => 'user.validate'
]);

Route::get('/mcc/select', [
    'uses' => 'UserLoginController@mccSelection',
    'as'   => 'user.mcc'
]);

Route::get('/campaigns',[
    'uses' => 'CampaignManagementController@campaigns',
    'as'   => 'campaign.get'
]);

Auth::routes();

Route::group(['prefix' => 'user'], function(){

});