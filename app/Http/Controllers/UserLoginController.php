<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Google\Auth\CredentialsLoader;
use Google\Auth\OAuth2;
use Google\AdsApi\AdWords\AdWordsServices;
use Google\AdsApi\AdWords\AdWordsSessionBuilder;
use Google\AdsApi\Common\OAuth2TokenBuilder;
use LaravelGoogleAds\Services\AdWordsService;
use Google\AdsApi\AdWords\v201710\cm\CampaignService;
use Google\AdsApi\AdWords\v201710\cm\OrderBy;
use Google\AdsApi\AdWords\v201710\cm\Paging;
use Google\AdsApi\AdWords\v201710\cm\Selector;

use App\AccountHierarchy;
use App\AccountChanges;
use App\GetCampaigns;

class UserLoginController extends Controller
{
    protected $adWordsService;
    
    /**
     * @param AdWordsService $adWordsService
     */
    public function __construct(AdWordsService $adWordsService)
    {
        $this->adWordsService = $adWordsService;
    }

    public function index()
    {
        return view('userlogin');
    }

    public function oauth()
    {
        session_start();

        $oauth2 = new OAuth2([
            'authorizationUri' => 'https://accounts.google.com/o/oauth2/v2/auth',
            'tokenCredentialUri' => 'https://www.googleapis.com/oauth2/v4/token',
            'redirectUri' => route('user.mcc'),
            'clientId' => '89284183044-de48l6rml250po9ujt13gksqecpjfrhi.apps.googleusercontent.com',
            'clientSecret' => 'C7d45-qb-cUpLuOY1iZLERYO',
            'scope' => 'https://www.googleapis.com/auth/plus.login',
        ]);

        if (!isset($_GET['code'])) {
            // Create a 'state' token to prevent request forgery.
            // Store it in the session for later validation.
            $oauth2->setState(sha1(openssl_random_pseudo_bytes(1024)));
            $_SESSION['oauth2state'] = $oauth2->getState();
            
            // Redirect the user to the authorization URL.
            $config = [
                // Set to 'offline' if you require offline access.
                'access_type' => 'offline'
            ];
            dd($oauth2);
            header('Location: ' . $oauth2->buildFullAuthorizationUri($config));
            exit;
        } 
        elseif (empty($_GET['state'])
        || ($_GET['state'] !== $_SESSION['oauth2state'])) 
        {
            unset($_SESSION['oauth2state']);
            exit('Invalid state.');
            } else {
            
            $oauth2->setCode($_GET['code']);
            $authToken = $oauth2->fetchAuthToken();
            // Store the refresh token for your user in your local storage if you
            // requested offline access.
            $refreshToken = $authToken['refresh_token'];

        }

    }

    public function validateLogin()
    {
        return redirect()->route('user.mcc');
    }

    public function mccSelection()
    {
        $mcc_accounts = new GetCampaigns();

        dd($mcc_accounts);

        // $campaignService = $this->adWordsService->getService(CampaignService::class);

        // // Create selector.
        // $selector = new Selector();
        // $selector->setFields(array('Id', 'Name'));
        // $selector->setOrdering(array(new OrderBy('Name', 'ASCENDING')));

        // // Create paging controls.
        // $selector->setPaging(new Paging(0, 100));

        // // Make the get request.
        // $page = $campaignService->get($selector);
    }

    /**
     * @var string the Google OAuth2 authorization URI for OAuth2 requests
     * @see https://developers.google.com/identity/protocols/OAuth2InstalledApp#formingtheurl
     */
    const AUTHORIZATION_URI = 'https://accounts.google.com/o/oauth2/v2/auth';
    /**
     * @var string the OAuth2 scope for the AdWords API
     * @see https://developers.google.com/adwords/api/docs/guides/authentication#scope
     */
    const ADWORDS_API_SCOPE = 'https://www.googleapis.com/auth/adwords';
    /**
     * @var string the OAuth2 scope for the DFP API
     * @see https://developers.google.com/doubleclick-publishers/docs/authentication#scope
     */
    const DFP_API_SCOPE = 'https://www.googleapis.com/auth/dfp';
    /**
     * @var string the redirect URI for OAuth2 installed application flows
     * @see https://developers.google.com/identity/protocols/OAuth2InstalledApp#formingtheurl
     */
    const REDIRECT_URI = 'urn:ietf:wg:oauth:2.0:oob';
    /**
     * Fetch auth token
     *
     * @param OAuth2 $oAuth2
     * @param string $code
     * @return array
     */
    public function fetchAuthToken(Oauth2 $oAuth2, $code)
    {
        $oAuth2->setCode($code);
        return $oAuth2->fetchAuthToken();
    }
    /**
     * Build url
     *
     * @param OAuth2 $oAuth2
     * @param bool $offlineAccess
     * @param array $params
     * @return \Psr\Http\Message\UriInterface
     */
    public function buildFullAuthorizationUri(OAuth2 $oAuth2, $offlineAccess = false, $params = [])
    {
        $defaults = [];
        $params = array_merge(
            $defaults,
            $params
        );
        if ($offlineAccess) {
            $params['access_type'] = 'offline';
        }
        return $oAuth2->buildFullAuthorizationUri($params);
    }
    /**
     * @param string $clientId
     * @param string $clientSecret
     * @param string|null $redirectUri
     * @param string|null $scope
     * @return OAuth2
     */
    public function oauth2($clientId = null, $clientSecret = null, $redirectUri = null, $scope = null)
    {
        $credentials = $this->credentials();
        $clientId = ($clientId) ?: $credentials['clientId'];
        $clientSecret = ($clientSecret) ?: $credentials['clientSecret'];
        $scope = ($scope) ?: self::ADWORDS_API_SCOPE;
        $redirectUri = ($redirectUri) ?: self::REDIRECT_URI;
        return new OAuth2([
            'authorizationUri' => self::AUTHORIZATION_URI,
            'redirectUri' => $redirectUri,
            'tokenCredentialUri' => CredentialsLoader::TOKEN_CREDENTIAL_URI,
            'clientId' => $clientId,
            'clientSecret' => $clientSecret,
            'scope' => $scope,
        ]);
    }
    /**
     * Credentials
     *
     * @return bool|mixed
     */
    private function credentials()
    {
        /** @var null|array $config */
        $config = config('google-ads');
        if (is_null($config) || !count($config)) {
            return false;
        }
        return array_merge([
            'clientId' => null,
            'clientSecret' => null,
        ], $config['OAUTH2']);
    }
}
